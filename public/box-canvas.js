import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

// Get the canvas element
const boxCanvas = document.getElementById('box-canvas');

// Set up Three.js renderer
const renderer = new THREE.WebGLRenderer({
  alpha: true,
  antialias: true,
  canvas: boxCanvas
});
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));

// Set up Three.js scene
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 10, 1000);
camera.position.set(40, 90, 110);

// Set up controls
const orbitControls = new OrbitControls(camera, boxCanvas);
orbitControls.enableZoom = false;
orbitControls.enablePan = false;
orbitControls.enableDamping = true;
orbitControls.autoRotate = true;
orbitControls.autoRotateSpeed = 0.25;

// Add some geometry to the scene
const geometry = new THREE.BoxGeometry(10, 10, 10);
const material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
const cube = new THREE.Mesh(geometry, material);
scene.add(cube);

// Set up render loop
function render() {
  renderer.render(scene, camera);
  requestAnimationFrame(render);
}

// Render the scene
render();

// Handle window resize
function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
}

window.addEventListener('resize', onWindowResize);