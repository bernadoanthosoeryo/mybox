import React, { useEffect } from 'react';

export default function Controls({ box, onParamsChange }) {
  useEffect(() => {
    // Import lil-gui only if running in the browser
    if (typeof window !== 'undefined') {
      import('three/addons/libs/lil-gui.module.min.js').then(lilGui => {
        const gui = new lilGui.GUI();
        const paramsFolder = gui.addFolder('Box Parameters');

        paramsFolder.add(boxParams, 'width', boxParams.widthLimits[0], boxParams.widthLimits[1]).onChange(onParamsChange);
        paramsFolder.add(boxParams, 'length', boxParams.lengthLimits[0], boxParams.lengthLimits[1]).onChange(onParamsChange);
        paramsFolder.add(boxParams, 'depth', boxParams.depthLimits[0], boxParams.depthLimits[1]).onChange(onParamsChange);
        paramsFolder.add(boxParams, 'fluteFreq', boxParams.fluteFreqLimits[0], boxParams.fluteFreqLimits[1]).onChange(onParamsChange).name('flute');
        paramsFolder.add(boxParams, 'thickness', boxParams.thicknessLimits[0], boxParams.thicknessLimits[1]).onChange(onParamsChange);

        // Destroy the GUI when the component unmounts
        return () => {
          gui.destroy();
        };
      });
    }
  }, [boxParams, onParamsChange]);

  return null; // No visual component for Controls
}