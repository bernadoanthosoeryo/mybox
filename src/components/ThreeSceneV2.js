import React, { useEffect, useRef, useState } from 'react';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import gsap from 'gsap';
import ScrollTrigger from "gsap/dist/ScrollTrigger";
import { mergeBufferGeometries } from './bufferLib'

gsap.registerPlugin(ScrollTrigger);
let box = {
  params: {
      width: 27,
      widthLimits: [15, 70],
      length: 80,
      lengthLimits: [70, 120],
      depth: 45,
      depthLimits: [15, 70],
      thickness: .6,
      thicknessLimits: [.1, 1],
      fluteFreq: 5,
      fluteFreqLimits: [3, 7],
      flapGap: 1,
      copyrightSize: [27, 10]
  },
  els: {
      group: new THREE.Group(),
      backHalf: {
          width: {
              top: new THREE.Mesh(),
              side: new THREE.Mesh(),
              bottom: new THREE.Mesh(),
          },
          length: {
              top: new THREE.Mesh(),
              side: new THREE.Mesh(),
              bottom: new THREE.Mesh(),
          },
      },
      frontHalf: {
          width: {
              top: new THREE.Mesh(),
              side: new THREE.Mesh(),
              bottom: new THREE.Mesh(),
          },
          length: {
              top: new THREE.Mesh(),
              side: new THREE.Mesh(),
              bottom: new THREE.Mesh(),
          },
      }
  },
  animated: {
      openingAngle: .02 * Math.PI,
      flapAngles: {
          backHalf: {
              width: {
                  top: 0,
                  bottom: 0
              },
              length: {
                  top: 0,
                  bottom: 0
              },
          },
          frontHalf: {
              width: {
                  top: 0,
                  bottom: 0
              },
              length: {
                  top: 0,
                  bottom: 0
              },
          }
      }
  }
};

export default function ThreeScene({ boxParams }) {
  const boxCanvasRef = useRef(null);
  const containerRef = useRef(null);
  const zoomInRef = useRef(null); // Add ref for zoom in button
  const zoomOutRef = useRef(null); // Add ref for zoom out button
  const [is3DMode, setIs3DMode] = useState(true);

  let renderer, scene, camera, orbit, rayCaster, mouse, lightHolder;
  
  useEffect(() => {
    // Initialize Three.js scene
    ScrollTrigger.enable();
    
    initScene();
    window.addEventListener('resize', updateSceneSize);

    // Clean-up function
    return () => {
      window.removeEventListener('resize', updateSceneSize);
    };
  }, []);

  function updateSceneSize() {
    const container = containerRef.current;
    camera.aspect = container.clientWidth / container.clientHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(container.clientWidth, container.clientHeight);
  }


  function render() {
    orbit.update();
    lightHolder.quaternion.copy(camera.quaternion);
    renderer.render(scene, camera);
    requestAnimationFrame(render);
  }

  function createLights() {
    const ambient = new THREE.AmbientLight(0x333333)
    const white = new THREE.PointLight(0xffffff, 2, 48)
    white.position.set(16, 16, 16)
    white.shadow.mapSize.width = 1024
    white.shadow.mapSize.height = 1024
    white.castShadow = true
  
    const red = new THREE.PointLight(0xff8888,  1, 64)
    red.position.set(-16, -16, -16)
    red.shadow.mapSize.width = 1024
    red.shadow.mapSize.height = 1024
    red.castShadow = true
  
    const green = new THREE.PointLight(0x88ff88,  2, 48)
    green.position.set(16, 16, -16)
    green.shadow.mapSize.width = 1024
    green.shadow.mapSize.height = 1024
    green.castShadow = true
  
    const blue = new THREE.PointLight(0x8888ff,  2, 48)
    blue.position.set(-16, 16, 16)
    blue.shadow.mapSize.width = 1024
    blue.shadow.mapSize.height = 1024
    blue.castShadow = true
    return [ambient, white, red, green, blue]
  }

  function initScene() {
    renderer = new THREE.WebGLRenderer({
      alpha: true,
      antialias: true,
      canvas: boxCanvasRef.current
    });
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
  
    // const canvasWidth = containerRef.current.clientWidth * window.devicePixelRatio;
    // const canvasHeight = containerRef.current.clientHeight * window.devicePixelRatio;
    const canvasWidth = containerRef.current.clientWidth;
    const canvasHeight = containerRef.current.clientHeight;
  
    // renderer.setSize(containerRef.current.clientWidth, containerRef.current.clientHeight);
  
    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(45, canvasWidth / canvasHeight, 10, 1000);
    camera.position.set(40, 90, 110);

    rayCaster = new THREE.Raycaster();
    mouse = new THREE.Vector2(0, 0);
   
    updateSceneSize();

    scene.add(box.els.group);
    setGeometryHierarchy();

    const ambientLight = new THREE.AmbientLight(0xffffff);
    scene.add(ambientLight);

    const sunlight = new THREE.DirectionalLight(0xffffff, 0.9);
    sunlight.position.set(0, 80000000, 100000000);
    sunlight.matrixWorldNeedsUpdate = true;
    scene.add(sunlight);

    lightHolder = new THREE.Group();
    const topLight = new THREE.PointLight(0xffffff, 100);
    topLight.position.set(-30, 300, 0);
    lightHolder.add(topLight);

    const sideLight = new THREE.PointLight(0xffffff, .7);
    sideLight.position.set(50, 0, 150);
    lightHolder.add(sideLight);
    
    scene.add(lightHolder);

    scene.add(box.els.group);
    setGeometryHierarchy();

    const material = new THREE.MeshStandardMaterial({
        color: new THREE.Color(0xffffff),
        side: THREE.DoubleSide,
        // metalness: 0.1,
        roughness: 0.5,
        // dithering: true,
        emissive: 0.3
    });
    box.els.group.traverse(c => {
        if (c.isMesh) c.material = material;
    });

    orbit = new OrbitControls(camera, boxCanvasRef.current);
    orbit.enableZoom = false;
    orbit.enablePan = false;
    orbit.enableDamping = true;
    orbit.autoRotate = true;
    orbit.autoRotateSpeed = .25;

    createBoxElements();
    createFoldingAnimation();
    createZooming();
    createControls();

    render();
  }

  function createZooming() {
    const zoomInBtn = zoomInRef.current;
    const zoomOutBtn = zoomOutRef.current;
  
    let zoomLevel = 1;
    const limits = [.4, 2];
  
    zoomInBtn.addEventListener('click', () => {
      zoomLevel *= 1.3;
      applyZoomLimits();
    });
    zoomOutBtn.addEventListener('click', () => {
      zoomLevel *= .75;
      applyZoomLimits();
    });
  
    function applyZoomLimits() {
      if (zoomLevel > limits[1]) {
        zoomLevel = limits[1];
        zoomInBtn.classList.add('disabled');
      } else if (zoomLevel < limits[0]) {
        zoomLevel = limits[0];
        zoomOutBtn.classList.add('disabled');
      } else {
        zoomInBtn.classList.remove('disabled');
        zoomOutBtn.classList.remove('disabled');
      }
      gsap.to(camera, {
        duration: .2,
        zoom: zoomLevel,
        onUpdate: () => {
          camera.updateProjectionMatrix();
        }
      })
    }
  }

  function createFoldingAnimation() {
    // scrollTrigger: {
    //   trigger: '.page',
    //   start: '0% 0%',
    //   end: '100% 100%',
    //   scrub: true,
    // },
    gsap.timeline({
      scrollTrigger: {
        trigger: '.page',
        start: '0% 0%',
        end: '+=100%', // Adjust this value to trigger animation earlier or later
        scrub: true,
      },
      onUpdate: () => {
        updatePanelsTransform();
      }
    })
    .to(box.animated, {
      duration: 1,
      openingAngle: 0.5 * Math.PI, // Angle for unfolded state
      ease: 'power1.inOut'
    })
    .to([box.animated.flapAngles.backHalf.width, box.animated.flapAngles.frontHalf.width], {
      duration: 0.6,
      bottom: 0.6 * Math.PI, // Angle for unfolded state
      ease: 'back.in(3)'
    }, 0.9)
    .to(box.animated.flapAngles.backHalf.length, {
      duration: 0.7,
      bottom: 0.5 * Math.PI, // Angle for unfolded state
      ease: 'back.in(2)'
    }, 1.1)
    .to(box.animated.flapAngles.frontHalf.length, {
      duration: 0.8,
      bottom: 0.49 * Math.PI, // Angle for unfolded state
      ease: 'back.in(3)'
    }, 1.4)
    .to([box.animated.flapAngles.backHalf.width, box.animated.flapAngles.frontHalf.width], {
      duration: 0.6,
      top: 0.6 * Math.PI, // Angle for unfolded state
      ease: 'back.in(3)'
    }, 1.4)
    .to(box.animated.flapAngles.backHalf.length, {
      duration: 0.7,
      top: 0.5 * Math.PI, // Angle for unfolded state
      ease: 'back.in(3)'
    }, 1.7)
    .to(box.animated.flapAngles.frontHalf.length, {
      duration: 0.9,
      top: 0.49 * Math.PI, // Angle for unfolded state
      ease: 'back.in(4)'
    }, 1.8);
  }

  function setGeometryHierarchy() {
    box.els.group.add(
        box.els.frontHalf.width.side, 
        box.els.frontHalf.length.side, 
        box.els.backHalf.width.side, 
        box.els.backHalf.length.side
    );
    box.els.frontHalf.width.side.add(
        box.els.frontHalf.width.top, 
        box.els.frontHalf.width.bottom
    );
    box.els.frontHalf.length.side.add(
        box.els.frontHalf.length.top, 
        box.els.frontHalf.length.bottom
    );
    box.els.backHalf.width.side.add(
        box.els.backHalf.width.top, 
        box.els.backHalf.width.bottom
    );
    box.els.backHalf.length.side.add(
        box.els.backHalf.length.top, 
        box.els.backHalf.length.bottom
    );
  }

  function createBoxElements() {
    for (let halfIdx = 0; halfIdx < 2; halfIdx++) {
      for (let sideIdx = 0; sideIdx < 2; sideIdx++) {

          const half = halfIdx ? 'frontHalf' : 'backHalf';
          const side = sideIdx ? 'width' : 'length';

          const sideWidth = side === 'width' ? box.params.width : box.params.length;
          const flapWidth = sideWidth - 2 * box.params.flapGap;
          const flapHeight = .5 * box.params.width - .75 * box.params.flapGap;

          const sidePlaneGeometry = new THREE.PlaneGeometry(
              sideWidth,
              box.params.depth,
              Math.floor(5 * sideWidth),
              Math.floor(.2 * box.params.depth)
          );
          const flapPlaneGeometry = new THREE.PlaneGeometry(
              flapWidth,
              flapHeight,
              Math.floor(5 * flapWidth),
              Math.floor(.2 * flapHeight)
          );

          const sideGeometry = createSideGeometry(
              sidePlaneGeometry,
              [sideWidth, box.params.depth],
              [true, true, true, true],
              false
          );
          const topGeometry = createSideGeometry(
              flapPlaneGeometry,
              [flapWidth, flapHeight],
              [false, false, true, false],
              true
          );
          const bottomGeometry = createSideGeometry(
              flapPlaneGeometry,
              [flapWidth, flapHeight],
              [true, false, false, false],
              true
          );

          topGeometry.translate(0, .5 * flapHeight, 0);
          bottomGeometry.translate(0, -.5 * flapHeight, 0);

          box.els[half][side].top.geometry = topGeometry;
          box.els[half][side].side.geometry = sideGeometry;
          box.els[half][side].bottom.geometry = bottomGeometry;

          box.els[half][side].top.position.y = .5 * box.params.depth;
          box.els[half][side].bottom.position.y = -.5 * box.params.depth;
      }
    }

    updatePanelsTransform();
  }

  function createSideGeometry(baseGeometry, size, folds, hasMiddleLayer) {
    const geometriesToMerge = [];
    geometriesToMerge.push(getLayerGeometry(v =>
        -.5 * box.params.thickness + .01 * Math.sin(box.params.fluteFreq * v)
    ));
    geometriesToMerge.push(getLayerGeometry(v =>
        .5 * box.params.thickness + .01 * Math.sin(box.params.fluteFreq * v)
    ));
    if (hasMiddleLayer) {
        geometriesToMerge.push(getLayerGeometry(v =>
            .5 * box.params.thickness * Math.sin(box.params.fluteFreq * v)
        ));
    }

    function getLayerGeometry(offset) {
        const layerGeometry = baseGeometry.clone();
        const positionAttr = layerGeometry.attributes.position;
        for (let i = 0; i < positionAttr.count; i++) {
            const x = positionAttr.getX(i);
            const y = positionAttr.getY(i)
            let z = positionAttr.getZ(i) + offset(x);
            z = applyFolds(x, y, z);
            positionAttr.setXYZ(i, x, y, z);
        }
        return layerGeometry;
    }

    function applyFolds(x, y, z) {
        let modifier = (c, s) => (1. - Math.pow(c / (.5 * s), 10.));
        if ((x > 0 && folds[1]) || (x < 0 && folds[3])) {
            z *= modifier(x, size[0]);
        }
        if ((y > 0 && folds[0]) || (y < 0 && folds[2])) {
            z *= modifier(y, size[1]);
        }
        return z;
    }

    const mergedGeometry = new mergeBufferGeometries(geometriesToMerge, false);
    mergedGeometry.computeVertexNormals();

    return mergedGeometry;
  }

  function updatePanelsTransform() {
    // place width-sides aside of length-sides (not animated)
    box.els.frontHalf.width.side.position.x = .5 * box.params.length;
    box.els.backHalf.width.side.position.x = -.5 * box.params.length;

    // rotate width-sides from 0 to 90 deg
    box.els.frontHalf.width.side.rotation.y = box.animated.openingAngle;
    box.els.backHalf.width.side.rotation.y = box.animated.openingAngle;

    // move length-sides to keep the box centered
    const cos = Math.cos(box.animated.openingAngle); // animates from 1 to 0
    box.els.frontHalf.length.side.position.x = -.5 * cos * box.params.width;
    box.els.backHalf.length.side.position.x = .5 * cos * box.params.width;

    // move length-sides to define box inner space
    const sin = Math.sin(box.animated.openingAngle); // animates from 0 to 1
    box.els.frontHalf.length.side.position.z = .5 * sin * box.params.width;
    box.els.backHalf.length.side.position.z = -.5 * sin * box.params.width;

    box.els.frontHalf.width.top.rotation.x = -box.animated.flapAngles.frontHalf.width.top;
    box.els.frontHalf.length.top.rotation.x = -box.animated.flapAngles.frontHalf.length.top;
    box.els.frontHalf.width.bottom.rotation.x = box.animated.flapAngles.frontHalf.width.bottom;
    box.els.frontHalf.length.bottom.rotation.x = box.animated.flapAngles.frontHalf.length.bottom;

    box.els.backHalf.width.top.rotation.x = box.animated.flapAngles.backHalf.width.top;
    box.els.backHalf.length.top.rotation.x = box.animated.flapAngles.backHalf.length.top;
    box.els.backHalf.width.bottom.rotation.x = -box.animated.flapAngles.backHalf.width.bottom;
    box.els.backHalf.length.bottom.rotation.x = -box.animated.flapAngles.backHalf.length.bottom;
  }

  function createControls() {
    import('three/addons/libs/lil-gui.module.min.js').then(lilGui => {
        const gui = new lilGui.GUI();
        gui.add(box.params, 'width', box.params.widthLimits[0], box.params.widthLimits[1]).step(1).onChange(() => {
            createBoxElements();
            updatePanelsTransform();
        });
        gui.add(box.params, 'length', box.params.lengthLimits[0], box.params.lengthLimits[1]).step(1).onChange(() => {
            createBoxElements();
            updatePanelsTransform();
        });
        gui.add(box.params, 'depth', box.params.depthLimits[0], box.params.depthLimits[1]).step(1).onChange(() => {
            createBoxElements();
            updatePanelsTransform();
        });
        gui.add(box.params, 'fluteFreq', box.params.fluteFreqLimits[0], box.params.fluteFreqLimits[1]).step(1).onChange(() => {
            createBoxElements();
        }).name('flute');
        gui.add(box.params, 'thickness', box.params.thicknessLimits[0], box.params.thicknessLimits[1]).step(.05).onChange(() => {
            createBoxElements();
        });
        gui.add({ toggleMode: toggleMode }, 'toggleMode').name('Toggle Mode');

        // Destroy the GUI when the component unmounts
        return () => {
            gui.destroy();
        };
    });
  }

  function toggleMode(){
    setIs3DMode(prevMode => !prevMode); 
  }

  return (
    <div class="content">
      <div class="page">
        <div ref={containerRef} className="container">
          <canvas ref={boxCanvasRef} id="box-canvas" />
          <div class="ui-controls">
            <button class="unbutton ui-controls__button" ref={zoomInRef} id="zoom-in" aria-label="Zoom in">+</button>
            <button class="unbutton ui-controls__button" ref={zoomOutRef} id="zoom-out" aria-label="Zoom out">-</button>
            <div>Scroll to animate</div>
          </div>
        </div>
      </div>
    </div>
     
  );
}