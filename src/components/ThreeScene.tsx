import React, { useEffect, useRef, useState } from 'react';
import * as THREE from 'three';
import { OrbitControls } from './OrbitControls';
import gsap from 'gsap';
import ScrollTrigger from "gsap/dist/ScrollTrigger";
import { mergeBufferGeometries } from './bufferLib';

gsap.registerPlugin(ScrollTrigger);

interface BoxParams {
  width: number;
  widthLimits: [number, number];
  length: number;
  lengthLimits: [number, number];
  depth: number;
  depthLimits: [number, number];
  thickness: number;
  thicknessLimits: [number, number];
  fluteFreq: number;
  fluteFreqLimits: [number, number];
  flapGap: number;
}

interface Animated {
  openingAngle: number;
  flapAngles: {
    backHalf: {
      width: { top: number; bottom: number };
      length: { top: number; bottom: number };
    };
    frontHalf: {
      width: { top: number; bottom: number };
      length: { top: number; bottom: number };
    };
  };
}

interface ImageState {
  src: string;
  position: { x: number; y: number };
}

const box: { params: BoxParams; els: any; animated: Animated } = {
  params: {
    width: 27,
    widthLimits: [15, 70],
    length: 80,
    lengthLimits: [70, 120],
    depth: 45,
    depthLimits: [15, 70],
    thickness: 0.6,
    thicknessLimits: [0.1, 1],
    fluteFreq: 5,
    fluteFreqLimits: [3, 7],
    flapGap: 1,
  },
  els: {
    group: new THREE.Group(),
    backHalf: {
      width: {
        top: new THREE.Mesh(),
        side: new THREE.Mesh(),
        bottom: new THREE.Mesh(),
      },
      length: {
        top: new THREE.Mesh(),
        side: new THREE.Mesh(),
        bottom: new THREE.Mesh(),
      },
    },
    frontHalf: {
      width: {
        top: new THREE.Mesh(),
        side: new THREE.Mesh(),
        bottom: new THREE.Mesh(),
      },
      length: {
        top: new THREE.Mesh(),
        side: new THREE.Mesh(),
        bottom: new THREE.Mesh(),
      },
    },
  },
  animated: {
    openingAngle: 0.02 * Math.PI,
    flapAngles: {
      backHalf: {
        width: { top: 0, bottom: 0 },
        length: { top: 0, bottom: 0 },
      },
      frontHalf: {
        width: { top: 0, bottom: 0 },
        length: { top: 0, bottom: 0 },
      },
    },
  },
};

export default function ThreeScene({ boxParams }: { boxParams: BoxParams }) {
  const boxCanvasRef = useRef<HTMLCanvasElement>(null);
  const containerRef = useRef<HTMLDivElement>(null);
  const zoomInRef = useRef<HTMLButtonElement>(null); // Add ref for zoom in button
  const zoomOutRef = useRef<HTMLButtonElement>(null); // Add ref for zoom out button
  const [uploadedImage, setUploadedImage] = useState<string | null>(null);
  const [is3DMode, setIs3DMode] = useState(true);
  const uploadInputRef = useRef<HTMLInputElement>(null);
  const [imagePosition, setImagePosition] = useState({ x: 0, y: 0 });


  let renderer: THREE.WebGLRenderer,
    scene: THREE.Scene,
    camera: THREE.PerspectiveCamera,
    orbit: OrbitControls,
    rayCaster: THREE.Raycaster,
    mouse: THREE.Vector2,
    customImage: THREE.Mesh
    lightHolder: THREE.Group;

  useEffect(() => {
    // Initialize Three.js scene
    ScrollTrigger.enable();

    initScene();
    window.addEventListener('resize', updateSceneSize);

    // Clean-up function
    return () => {
      window.removeEventListener('resize', updateSceneSize);
    };
  }, []);

  function updateSceneSize() {
    const container = containerRef.current!;
    camera.aspect = container.clientWidth / container.clientHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(container.clientWidth, container.clientHeight);
  }

  function render() {
    orbit.update();
    // lightHolder.quaternion.copy(camera.quaternion);
    renderer.render(scene, camera);
    requestAnimationFrame(render);
  }

  function createLights(): void {
    const ambientLight = new THREE.AmbientLight(0xffffff);
    scene.add(ambientLight);

    const directionalLight = new THREE.DirectionalLight(0xffffff, 3);
    const directionalLight2 = new THREE.DirectionalLight(0xffffff, 3);

    directionalLight.position.set(3, 3, 3);
    directionalLight2.position.set(-3, -3, -3);

    scene.add(ambientLight);
    scene.add(directionalLight);
    scene.add(directionalLight2);

    // temporary disable shadow light
    // lightHolder = new THREE.Group();
    // const topLight = new THREE.PointLight(0xffffff, 100);
    // topLight.position.set(-30, 300, 0);
    // lightHolder.add(topLight);

    // const sideLight = new THREE.PointLight(0xffffff, 0.7);
    // sideLight.position.set(50, 0, 150);
    // lightHolder.add(sideLight);

    // scene.add(lightHolder);
  }

  function setOrbit(): void {
    orbit = new OrbitControls(camera, boxCanvasRef.current!);
    orbit.enableZoom = false;
    orbit.enablePan = false;
    orbit.enableDamping = true;
    orbit.autoRotate = true;
    orbit.autoRotateSpeed = 0.25;
  }

  function initScene() {
    renderer = new THREE.WebGLRenderer({
      alpha: true,
      antialias: true,
      canvas: boxCanvasRef.current!,
    });
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));

    const canvasWidth = containerRef.current!.clientWidth;
    const canvasHeight = containerRef.current!.clientHeight;

    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(45, canvasWidth / canvasHeight, 10, 1000);
    camera.position.set(40, 90, 110);

    rayCaster = new THREE.Raycaster();
    mouse = new THREE.Vector2(0, 0);

    updateSceneSize();

    scene.add(box.els.group);
    setGeometryHierarchy();

    createLights();

    scene.add(box.els.group);
    setGeometryHierarchy();

    const material = new THREE.MeshStandardMaterial({
      color: new THREE.Color(0xC7B7A3),
      side: THREE.DoubleSide,
      metalness: 0.1,
      roughness: 0.4,
      dithering: true,
      emissive: 0.3
    });
    box.els.group.traverse((c: any) => {
      if (c.isMesh) c.material = material;
    });

    setOrbit();

    createCustomImage()
    createBoxElements();
    createFoldingAnimation();
    createZooming();
    createControls();

    render();
  }

  function createZooming() {
    const zoomInBtn = zoomInRef.current!;
    const zoomOutBtn = zoomOutRef.current!;

    let zoomLevel = 1;
    const limits = [0.4, 2];

    zoomInBtn.addEventListener('click', () => {
      zoomLevel *= 1.3;
      applyZoomLimits();
    });
    zoomOutBtn.addEventListener('click', () => {
      zoomLevel *= 0.75;
      applyZoomLimits();
    });

    function applyZoomLimits() {
      if (zoomLevel > limits[1]) {
        zoomLevel = limits[1];
        zoomInBtn.classList.add('disabled');
      } else if (zoomLevel < limits[0]) {
        zoomLevel = limits[0];
        zoomOutBtn.classList.add('disabled');
      } else {
        zoomInBtn.classList.remove('disabled');
        zoomOutBtn.classList.remove('disabled');
      }
      gsap.to(camera, {
        duration: 0.2,
        zoom: zoomLevel,
        onUpdate: () => {
          camera.updateProjectionMatrix();
        },
      });
    }
  }

  function createFoldingAnimation() {
    gsap.timeline({
      scrollTrigger: {
        trigger: '.page',
        start: '0% 0%',
        end: '+=100%', // Adjust this value to trigger animation earlier or later
        scrub: true,
      },
      onUpdate: () => {
        updatePanelsTransform();
        checkCustomImangeIntersect();
      },
    })
      .to(box.animated, {
        duration: 1,
        openingAngle: 0.5 * Math.PI, // Angle for unfolded state
        ease: 'power1.inOut',
      })
      .to([box.animated.flapAngles.backHalf.width, box.animated.flapAngles.frontHalf.width], {
        duration: 0.6,
        bottom: 0.6 * Math.PI, // Angle for unfolded state
        ease: 'back.in(3)',
      }, 0.9)
      .to(box.animated.flapAngles.backHalf.length, {
        duration: 0.7,
        bottom: 0.5 * Math.PI, // Angle for unfolded state
        ease: 'back.in(2)',
      }, 1.1)
      .to(box.animated.flapAngles.frontHalf.length, {
        duration: 0.8,
        bottom: 0.49 * Math.PI, // Angle for unfolded state
        ease: 'back.in(3)',
      }, 1.4)
      .to([box.animated.flapAngles.backHalf.width, box.animated.flapAngles.frontHalf.width], {
        duration: 0.6,
        top: 0.6 * Math.PI, // Angle for unfolded state
        ease: 'back.in(3)',
      }, 1.4)
      .to(box.animated.flapAngles.backHalf.length, {
        duration: 0.7,
        top: 0.5 * Math.PI, // Angle for unfolded state
        ease: 'back.in(3)',
      }, 1.7)
      .to(box.animated.flapAngles.frontHalf.length, {
        duration: 0.9,
        top: 0.49 * Math.PI, // Angle for unfolded state
        ease: 'back.in(4)',
      }, 1.8);
  }

  function setGeometryHierarchy() {
    box.els.group.add(
      box.els.frontHalf.width.side,
      box.els.frontHalf.length.side,
      box.els.backHalf.width.side,
      box.els.backHalf.length.side
    );
    box.els.frontHalf.width.side.add(box.els.frontHalf.width.top, box.els.frontHalf.width.bottom);
    box.els.frontHalf.length.side.add(box.els.frontHalf.length.top, box.els.frontHalf.length.bottom);
    box.els.backHalf.width.side.add(box.els.backHalf.width.top, box.els.backHalf.width.bottom);
    box.els.backHalf.length.side.add(box.els.backHalf.length.top, box.els.backHalf.length.bottom);
  }

  function createBoxElements() {
    for (let halfIdx = 0; halfIdx < 2; halfIdx++) {
      for (let sideIdx = 0; sideIdx < 2; sideIdx++) {
        const half = halfIdx ? 'frontHalf' : 'backHalf';
        const side = sideIdx ? 'width' : 'length';

        const sideWidth = side === 'width' ? box.params.width : box.params.length;
        const flapWidth = sideWidth - 2 * box.params.flapGap;
        const flapHeight = 0.5 * box.params.width - 0.75 * box.params.flapGap;

        const sidePlaneGeometry = new THREE.PlaneGeometry(sideWidth, box.params.depth, Math.floor(5 * sideWidth), Math.floor(0.2 * box.params.depth));
        const flapPlaneGeometry = new THREE.PlaneGeometry(flapWidth, flapHeight, Math.floor(5 * flapWidth), Math.floor(0.2 * flapHeight));

        const sideGeometry = createSideGeometry(sidePlaneGeometry, [sideWidth, box.params.depth], [true, true, true, true], false);
        const topGeometry = createSideGeometry(flapPlaneGeometry, [flapWidth, flapHeight], [false, false, true, false], true);
        const bottomGeometry = createSideGeometry(flapPlaneGeometry, [flapWidth, flapHeight], [true, false, false, false], true);

        topGeometry.translate(0, 0.5 * flapHeight, 0);
        bottomGeometry.translate(0, -0.5 * flapHeight, 0);

        box.els[half][side].top.geometry = topGeometry;
        box.els[half][side].side.geometry = sideGeometry;
        box.els[half][side].bottom.geometry = bottomGeometry;

        box.els[half][side].top.position.y = 0.5 * box.params.depth;
        box.els[half][side].bottom.position.y = -0.5 * box.params.depth;
      }
    }

    updatePanelsTransform();
  }

  function createSideGeometry(baseGeometry: THREE.PlaneGeometry, size: [number, number], folds: boolean[], hasMiddleLayer: boolean): THREE.BufferGeometry {
    const geometriesToMerge: THREE.Geometry[] = [];
    geometriesToMerge.push(getLayerGeometry(v => -0.5 * box.params.thickness + 0.01 * Math.sin(box.params.fluteFreq * v)));
   
    geometriesToMerge.push(getLayerGeometry(v => 0.5 * box.params.thickness + 0.01 * Math.sin(box.params.fluteFreq * v)));
    if (hasMiddleLayer) {
      geometriesToMerge.push(getLayerGeometry(v => 0.5 * box.params.thickness * Math.sin(box.params.fluteFreq * v)));
    }

    function getLayerGeometry(offset: (v: number) => number): THREE.BufferGeometry {
      const layerGeometry = baseGeometry.clone();
      const positionAttr = layerGeometry.attributes.position!;
      for (let i = 0; i < positionAttr.count; i++) {
        const x = positionAttr.getX(i);
        const y = positionAttr.getY(i);
        let z = positionAttr.getZ(i) + offset(x);
        z = applyFolds(x, y, z);
        positionAttr.setXYZ(i, x, y, z);
      }
      return layerGeometry;
    }

    function applyFolds(x: number, y: number, z: number): number {
      const modifier = (c: number, s: number) => (1.0 - Math.pow(c / (0.5 * s), 10.0));
      if ((x > 0 && folds[1]) || (x < 0 && folds[3])) {
        z *= modifier(x, size[0]);
      }
      if ((y > 0 && folds[0]) || (y < 0 && folds[2])) {
        z *= modifier(y, size[1]);
      }
      return z;
    }

    const mergedGeometry = new mergeBufferGeometries(geometriesToMerge, false);
    mergedGeometry.computeVertexNormals();

    return mergedGeometry;
  }

  function updatePanelsTransform() {
    // place width-sides aside of length-sides (not animated)
    box.els.frontHalf.width.side.position.x = 0.5 * box.params.length;
    box.els.backHalf.width.side.position.x = -0.5 * box.params.length;

    // rotate width-sides from 0 to 90 deg
    box.els.frontHalf.width.side.rotation.y = box.animated.openingAngle;
    box.els.backHalf.width.side.rotation.y = box.animated.openingAngle;

    // move length-sides to keep the box centered
    const cos = Math.cos(box.animated.openingAngle); // animates from 1 to 0
    box.els.frontHalf.length.side.position.x = -0.5 * cos * box.params.width;
    box.els.backHalf.length.side.position.x = 0.5 * cos * box.params.width;

    // move length-sides to define box inner space
    const sin = Math.sin(box.animated.openingAngle); // animates from 0 to 1
    box.els.frontHalf.length.side.position.z = 0.5 * sin * box.params.width;
    box.els.backHalf.length.side.position.z = -0.5 * sin * box.params.width;

    box.els.frontHalf.width.top.rotation.x = -box.animated.flapAngles.frontHalf.width.top;
    box.els.frontHalf.length.top.rotation.x = -box.animated.flapAngles.frontHalf.length.top;
    box.els.frontHalf.width.bottom.rotation.x = box.animated.flapAngles.frontHalf.width.bottom;
    box.els.frontHalf.length.bottom.rotation.x = box.animated.flapAngles.frontHalf.length.bottom;

    box.els.backHalf.width.top.rotation.x = box.animated.flapAngles.backHalf.width.top;
    box.els.backHalf.length.top.rotation.x = box.animated.flapAngles.backHalf.length.top;
    box.els.backHalf.width.bottom.rotation.x = -box.animated.flapAngles.backHalf.width.bottom;
    box.els.backHalf.length.bottom.rotation.x = -box.animated.flapAngles.backHalf.length.bottom;

    if (uploadedImage) {
      customImage.position.copy(box.els.frontHalf.length.side.position);
      customImage.position.x += 0.5 * box.params.length - 0.5 * 27;
      customImage.position.y -= 0.5 * (box.params.depth - 10);
      customImage.position.z += box.params.thickness;
    }
   
  }

  function createControls() {
    import('three/addons/libs/lil-gui.module.min.js').then((lilGui: any) => {
      const gui = new lilGui.GUI();
      gui.add(box.params, 'width', box.params.widthLimits[0], box.params.widthLimits[1]).step(1).onChange(() => {
        createBoxElements();
        updatePanelsTransform();
      });
      gui.add(box.params, 'length', box.params.lengthLimits[0], box.params.lengthLimits[1]).step(1).onChange(() => {
        createBoxElements();
        updatePanelsTransform();
      });
      gui.add(box.params, 'depth', box.params.depthLimits[0], box.params.depthLimits[1]).step(1).onChange(() => {
        createBoxElements();
        updatePanelsTransform();
      });
      gui.add(box.params, 'fluteFreq', box.params.fluteFreqLimits[0], box.params.fluteFreqLimits[1]).step(1).onChange(() => {
        createBoxElements();
      }).name('flute');
      gui.add(box.params, 'thickness', box.params.thicknessLimits[0], box.params.thicknessLimits[1]).step(0.05).onChange(() => {
        createBoxElements();
      });
      gui.add({ toggleMode: toggleMode }, 'toggleMode').name('Toggle Mode');

      // Destroy the GUI when the component unmounts
      return () => {
        gui.destroy();
      };
    });
  }

  function toggleMode() {
    setIs3DMode(prevMode => !prevMode);
  }

  function handleFileUpload(event: React.ChangeEvent<HTMLInputElement>) {
    const file = event.target.files?.[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = function (e) {
        const imgData = e.target?.result as string;
        setUploadedImage(imgData);
        createImageMesh(imgData);
      };
      reader.readAsDataURL(file);
    }
  }

  function createCustomImage() {
    const canvas = document.createElement('canvas');
    canvas.width = 27 * 10;
    canvas.height = 10 * 10;
    const planeGeometry = new THREE.PlaneGeometry(27, 10);

    const ctx = canvas.getContext('2d');
    if (ctx) {
        ctx.clearRect(0, 0, canvas.width, canvas.width);
        ctx.fillStyle = '#000000';
        ctx.font = '22px sans-serif';
        ctx.textAlign = 'end';
        ctx.fillText('Kardoos', canvas.width - 30, 30);
        ctx.fillText('Custom Editor V2', canvas.width - 30, 70);

        ctx.lineWidth = 2;
        ctx.beginPath();
        ctx.moveTo(canvas.width - 160, 35);
        ctx.lineTo(canvas.width - 30, 35);
        ctx.stroke();
        ctx.beginPath();
        ctx.moveTo(canvas.width - 228, 77);
        ctx.lineTo(canvas.width - 30, 77);
        ctx.stroke();

        const texture = new THREE.CanvasTexture(canvas);
        customImage = new THREE.Mesh(planeGeometry, new THREE.MeshBasicMaterial({
            map: texture,
            transparent: true,
            opacity: 0.5
        }));

        scene.add(customImage);
        trackLinks();
    }
  }

  function createImageMesh(imgData: string) {
    const textureLoader = new THREE.TextureLoader();
    textureLoader.load(
      imgData,
      (texture) => {

        // const boxWidth = box.params.width;
        // const boxLength = box.params.length;
        // const geometry = new THREE.PlaneGeometry(boxWidth * 0.2, boxLength * 0.2); // Adjust size as needed

        // const material = new THREE.MeshBasicMaterial({ map: texture, side: THREE.DoubleSide, transparent: true });
        // const mesh = new THREE.Mesh(geometry, material);

        // mesh.position.copy(box.els.frontHalf.length.side.position);
        // mesh.position.x += .5 * box.params.length - .5 * 27;
        // mesh.position.y -= .5 * (box.params.depth - 10);
        // mesh.position.z += box.params.thickness;

        // // Position the image mesh at the center of the mailer box
        // mesh.position.set(0, 0, box.params.depth / 2 + 0.1); // Adjust the z-offset to avoid z-fighting with the box

        // // Rotate the image mesh to align with the box orientation
        // mesh.rotation.set(-Math.PI / 2, 0, 0); // Rotate 90 degrees around the x-axis

        // scene.add(mesh);
        // ---test 2
        // const aspectRatio = texture.image.width / texture.image.height;
        // const planeGeometry = new THREE.PlaneGeometry(
        //     aspectRatio * 10,
        //     10
        // );

        // texture.flipY = false; // Flip the texture vertically if needed

        // const material = new THREE.MeshBasicMaterial({
        //     map: texture,
        //     transparent: true,
        //     opacity: 0.5
        // });

        // const CustomImange = new THREE.Mesh(planeGeometry, material);
        // scene.add(CustomImange);

        // trackLinks(mesh);
      },
      undefined,
      (error) => {
        console.error('An error occurred while loading the image:', error);
      }
    );
  }

  function trackLinks() {
    document.addEventListener('mousemove', (e: any) => {
        updateMousePosition(e.clientX, e.clientY);
        checkCustomImangeIntersect();
    }, false);
    document.addEventListener('click', (e: any) => {
        updateMousePosition(
            e.targetTouches ? e.targetTouches[0].pageX : e.clientX,
            e.targetTouches ? e.targetTouches[0].pageY : e.clientY
        );
    });

    function updateMousePosition(x: any, y: any) {
      mouse.x = x / window.innerWidth * 2 - 1;
      mouse.y = -y / window.innerHeight * 2 + 1;
    }
  }

  function checkCustomImangeIntersect() {
    // if (uploadedImage) {
      let linkHovered = 0;
      rayCaster.setFromCamera(mouse, camera);
      const intersects = rayCaster.intersectObject(customImage);
      if (intersects.length) {
          document.body.style.cursor = 'pointer';
          // Check which part of the image was clicked based on UV coordinates
          linkHovered = intersects[0].uv.y > 0.5 ? 1 : 2;
      } else {
          document.body.style.cursor = 'auto';
      }
      return linkHovered;
    // }
}

  return (
    <div className="content">
      <div className="page">
        <div ref={containerRef} className="container">
          <canvas
            ref={boxCanvasRef}
            id="box-canvas" 
          />
          <div className="ui-controls">
            <button className="unbutton ui-controls__button" ref={zoomInRef} id="zoom-in" aria-label="Zoom in">+</button>
            <button className="unbutton ui-controls__button" ref={zoomOutRef} id="zoom-out" aria-label="Zoom out">-</button>
            <div>Scroll to animate</div>
            <input type="file" accept="image/*" onChange={handleFileUpload} ref={uploadInputRef} />
          </div>
        </div>
      </div>
    </div>
  );
}