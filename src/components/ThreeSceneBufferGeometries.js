import React, { useEffect, useRef } from 'react';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { mergeBufferGeometries } from './bufferLib'

export default function ThreeScene({ boxParams }) {
  const boxCanvasRef = useRef(null);
  const containerRef = useRef(null);
  let renderer, scene, camera, orbit;

  useEffect(() => {
    const boxCanvas = boxCanvasRef.current;
    const container = containerRef.current;

    // Initialize Three.js scene
    initScene();

    // Render loop
    function render() {
      renderer.render(scene, camera);
      requestAnimationFrame(render);
    }

    // Handle window resize
    function updateSceneSize() {
      camera.aspect = container.clientWidth / container.clientHeight;
      camera.updateProjectionMatrix();
      renderer.setSize(container.clientWidth, container.clientHeight);
    }

    window.addEventListener('resize', updateSceneSize);

    // Clean-up function
    return () => {
      window.removeEventListener('resize', updateSceneSize);
    };
    
    // Three.js scene setup
    function initScene() {
      renderer = new THREE.WebGLRenderer({
        alpha: true,
        antialias: true,
        canvas: boxCanvas
      });
      renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));

      scene = new THREE.Scene();
      camera = new THREE.PerspectiveCamera(45, container.clientWidth / container.clientHeight, 10, 1000);
      camera.position.set(40, 90, 110);

      orbit = new OrbitControls(camera, boxCanvas);
      orbit.enableZoom = false;
      orbit.enablePan = false;
      orbit.enableDamping = true;
      orbit.autoRotate = true;
      orbit.autoRotateSpeed = 0.25;

      // Apply transformations, merge geometries, etc.
      const geometry = createSideGeometry('width', boxParams);
      const material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
      const cube = new THREE.Mesh(geometry, material);
      scene.add(cube);

      // Add lighting
      const ambientLight = new THREE.AmbientLight(0xffffff, 0.5);
      scene.add(ambientLight);

      const directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);
      directionalLight.position.set(1, 1, 1);
      scene.add(directionalLight);

      // Start rendering loop after initializing everything
      render();
    }
  }, [boxParams]);

  // Function to create side geometry
  function createSideGeometry(side, params) {
    // Calculate side width and flap dimensions based on params
    const sideWidth = side === 'width' ? params.width : params.length;
    const flapWidth = sideWidth - 2 * params.flapGap;
    const flapHeight = 0.5 * params.width - 0.75 * params.flapGap;

    // Create plane geometries for side and flaps
    const sidePlaneGeometry = new THREE.PlaneGeometry(
      sideWidth,
      params.depth,
      Math.floor(5 * sideWidth),
      Math.floor(0.2 * params.depth)
    );
    const flapPlaneGeometry = new THREE.PlaneGeometry(
      flapWidth,
      flapHeight,
      Math.floor(5 * flapWidth),
      Math.floor(0.2 * flapHeight)
    );

    // Apply transformations to individual geometries
    // You can apply transformations here if needed

    // Merge the geometries into a single geometry
    const mergedGeometry = mergeBufferGeometries([sidePlaneGeometry, flapPlaneGeometry]);

    // Return the merged geometry
    return mergedGeometry;
  }

  return (
    <div ref={containerRef} className="container">
      <canvas ref={boxCanvasRef} id="box-canvas" />
    </div>
  );
}