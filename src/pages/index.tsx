import { useState } from 'react';
import Head from 'next/head';
import ThreeScene from '@/components/ThreeScene';

export default function Home() {
  const [boxParams, setBoxParams] = useState({
    width: 27,
    widthLimits: [15, 70],
    length: 80,
    lengthLimits: [70, 120],
    depth: 45,
    depthLimits: [15, 70],
    fluteFreq: 5,
    fluteFreqLimits: [3, 7],
    thickness: 0.6,
    thicknessLimits: [0.1, 1],
  });

  // Function to handle parameter changes
  const handleParamsChange = (changedParams) => {
    setBoxParams((prevParams) => ({
      ...prevParams,
      ...changedParams,
    }));
  };


  return (
    <>
      <Head>
        <title>Next.js Three.js App</title>
        <meta name="description" content="Next.js Three.js App" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <div className="custom-3d-container">
          {/* <Controls boxParams={boxParams} onParamsChange={handleParamsChange} /> */}
          <ThreeScene boxParams={boxParams} />
        </div>
      </main>
    </>
  );
}